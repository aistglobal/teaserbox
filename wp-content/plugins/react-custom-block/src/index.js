import {registerBlockType} from '@wordpress/blocks';
import image from './public/images/mainImage.png';
import './public/css/index.css'
import {Fragment} from "react";
import {TeaserBox, TeaserBoxInspector} from "./components";

registerBlockType('myguten/custom-block', {
    title: 'TeaserBox',
    icon: 'hammer',
    category: 'design',
    attributes: {
        title: {
            type: 'string',
            default: 'Demo Box',
        },
        description: {
            type: 'string',
            default: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry'
        },
        image: {
            type: 'string',
            default: image
        },
        bgColor: {
            type: 'string',
            default: '#f4f5f5'
        },
        showImage: {
            type: 'boolean',
            default: true
        },
        showLink: {
            type: 'boolean',
            default: true
        },
        linkText: {
            type: 'string',
            default: ''
        },
        linkHref: {
            type: 'string',
            default: '#'
        },
    },
    edit: (props) => {
        return (
            <Fragment>
                <TeaserBoxInspector {...props}/>
                <TeaserBox {...props}/>
            </Fragment>
        )

    },
    save: (props) => {
        return <TeaserBox {...props}/>
    },
});
