import {ArrowSvg} from './ArrowSvg'

export const TeaserBox = (props) => {
    const {attributes: {title, description, image, bgColor, showImage, linkText, linkHref, showLink}} = props;

    const titleColor = bgColor !== '#f4f5f5' ? '#fff' : '#23373C';
    const descriptionColor = bgColor !== '#f4f5f5' ? '#fff' : '#59686C';

    return (
        <div className={(showLink && linkHref && linkHref !== '#') ? 'TeaserBoxMain hover' : 'TeaserBoxMain'}>
            {showImage && (<div className="cardImage">
                <img src={image} alt={image}/>
            </div>)}
            <div className={'cardText'} style={{background: bgColor, marginTop: showImage ? '-60px' : 0}}>
                <p className={'title'} style={{color: titleColor}}>
                    {title}
                </p>
                <p className={'description'} style={{color: descriptionColor}}>
                    {description}
                </p>
                {showLink && (
                    <a href={linkHref} className={'link'} style={{color: titleColor}} target="_blank" rel="noopener">
                        {linkText}
                        <ArrowSvg
                            fill={'currentColor'}
                            height={18}
                            width={22}/>
                    </a>)}
            </div>
        </div>
    )
}
