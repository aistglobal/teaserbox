export const Uploader = (props) => {
    let frame
    const runUploader = (event) => {
        event.preventDefault()

        if (frame) {
            frame.open()
            return
        }

        frame = wp.media({
            title: 'Select or Upload Media Of Your Chosen Persuasion',
            button: {
                text: 'Use this media',
            },
            multiple: false,
            library: {
                type: 'image',
            }
        })

        frame.on('close', function () {
            const image = frame.state().get('selection').first().toJSON();
            props.setImage({image: image.url})

        })

        frame.open()
    }
    return (
        <button type='button' className={'uploaderBtn'} onClick={runUploader}>
            Choose Image
        </button>
    )
}
