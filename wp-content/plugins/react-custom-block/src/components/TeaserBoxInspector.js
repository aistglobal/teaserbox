import {PanelBody, SelectControl, TextControl, ToggleControl} from "@wordpress/components";
import {InspectorControls} from "@wordpress/block-editor";
import {Fragment} from "react";
import {Uploader} from "./Uploader";

export const TeaserBoxInspector = (props) => {
    const {
        setAttributes,
        attributes: {title, description, bgColor, showImage, linkText, linkHref, showLink}
    } = props

    return (
        <InspectorControls>
            <PanelBody title='Settings'>
                <TextControl
                    label='Title'
                    value={title}
                    onChange={(value) => setAttributes({title: value})}
                />

                <TextControl
                    label='Description'
                    value={description}
                    onChange={(value) => setAttributes({description: value})}
                />

                <SelectControl
                    label="Background Color"
                    value={bgColor}
                    options={[
                        {label: 'Dark', value: '#23373c'},
                        {label: 'Primary', value: '#c1002a'},
                        {label: 'Light', value: '#f4f5f5'},
                    ]}
                    onChange={(bgColor) => setAttributes({bgColor})}
                />

                <ToggleControl
                    label="With Image"
                    help={
                        showImage
                            ? 'Has Image.'
                            : 'No Image.'
                    }
                    checked={showImage}
                    onChange={() => {
                        setAttributes({showImage: !showImage});
                    }}
                />

                <ToggleControl
                    label="With Link"
                    help={
                        showLink
                            ? 'Has Link.'
                            : 'No Link.'
                    }
                    checked={showLink}
                    onChange={() => {
                        setAttributes({showLink: !showLink});
                    }}
                />

                {showLink && (<Fragment>
                    <TextControl
                        label='Link Text'
                        value={linkText}
                        onChange={(value) => setAttributes({linkText: value})}
                    />

                    <TextControl
                        label='Link Href'
                        value={linkHref}
                        onChange={(value) => setAttributes({linkHref: value})}
                    />
                </Fragment>)}

                <Uploader setImage={setAttributes}/>
            </PanelBody>
        </InspectorControls>
    )
}
